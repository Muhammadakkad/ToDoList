namespace ToDoList.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class todo4 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ToDoes", "IsAdmin", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ToDoes", "IsAdmin");
        }
    }
}
