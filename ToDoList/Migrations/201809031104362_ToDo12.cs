namespace ToDoList.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ToDo12 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ToDoes", "RowNo", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ToDoes", "RowNo");
        }
    }
}
