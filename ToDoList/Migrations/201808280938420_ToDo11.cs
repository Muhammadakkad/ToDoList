namespace ToDoList.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ToDo11 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ToDoes", "IsDone1", c => c.Boolean(nullable: false));
            AddColumn("dbo.ToDoes", "IsDone2", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ToDoes", "IsDone2");
            DropColumn("dbo.ToDoes", "IsDone1");
        }
    }
}
